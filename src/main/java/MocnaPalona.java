/**
 * Created by jan_w on 03.10.2017.
 */
public class MocnaPalona extends Napoj {

    public MocnaPalona() {
        opis = "Kawa Mocna Palona";
    }

    @Override
    public double koszt() {
        return 0.99;
    }
}
