/**
 * Created by jan_w on 03.10.2017.
 */
public class Czekolada extends SkladnikiDekorator {

    Napoj napoj;

    public Czekolada(Napoj napoj) {
        this.napoj = napoj;
    }

    @Override
    public String pobierzOpis() {
        return napoj.pobierzOpis() + ", Czekolada";
    }

    @Override
    public double koszt() {
        return napoj.koszt() + 0.20;
    }
}
