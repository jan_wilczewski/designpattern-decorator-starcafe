/**
 * Created by jan_w on 03.10.2017.
 */
public abstract class Napoj {

    String opis = "Napój nieznany";

    public String pobierzOpis(){
        return opis;
    }

    public abstract double koszt();
}
