/**
 * Created by jan_w on 03.10.2017.
 */
public class Bezkofeinowa extends Napoj {

    public Bezkofeinowa() {
        opis = "Kawa Bezkofeinowa";
    }

    @Override
    public double koszt() {
        return 1.05;
    }
}
