/**
 * Created by jan_w on 03.10.2017.
 */
public class BitaSmietana extends SkladnikiDekorator {

    Napoj napoj;

    public BitaSmietana(Napoj napoj) {
        this.napoj = napoj;
    }

    @Override
    public String pobierzOpis() {
        return napoj.pobierzOpis() + ", Bita śmietana";
    }

    @Override
    public double koszt() {
        return napoj.koszt() + 0.10;
    }
}
