/**
 * Created by jan_w on 03.10.2017.
 */
public class Espresso extends Napoj {

    public Espresso() {
        opis = "Kawa Espresso";
    }

    @Override
    public double koszt() {
        return 1.99;
    }
}
