/**
 * Created by jan_w on 03.10.2017.
 */
public class MleczkoSojowe extends SkladnikiDekorator {

    Napoj napoj;

    public MleczkoSojowe(Napoj napoj) {
        this.napoj = napoj;
    }

    @Override
    public String pobierzOpis() {
        return napoj.pobierzOpis() + ", Mleczko sojowe";
    }

    @Override
    public double koszt() {
        return napoj.koszt() + 0.15;
    }
}
