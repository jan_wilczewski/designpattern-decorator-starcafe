/**
 * Created by jan_w on 03.10.2017.
 */
public class StarCafeSpecial extends Napoj {

    public StarCafeSpecial() {
        opis = "Kawa Star Cafe Special";
    }

    @Override
    public double koszt() {
        return 0.89;
    }
}
